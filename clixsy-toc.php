<?php

/**
 * Plugin Name: clixsy ToC
 * Version: 1.0.2
 * Plugin URI: https://www.clixsy.com/
 * Description: Clixsy plugin to create table of content for pages/posts.
 * Author: Zakharchyshyn Bogdan
 * Author URI: https://www.clixsy.com/
 * Requires at least: 5.4.0
 * Tested up to: 5.9.2
 *
 * Text Domain: clixsy-toc
 * Domain Path: /lang/
 *
 * @package WordPress
 * @author Zakharchyshyn Bogdan
 * @since 1.0.0
 */

if (!defined('ABSPATH')) {
	exit;
}

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/zbogdan.work/toc',
	__FILE__, //Full path to the main plugin file or functions.php.
	'clixsy-toc'
);
$myUpdateChecker->setAuthentication('glpat-brMRr7NHRZoXhXG_s1vj');
$myUpdateChecker->setBranch('main');

// Load plugin class files.
require_once 'includes/class-clixsy-toc.php';
require_once 'includes/class-clixsy-toc-settings.php';

// Load plugin libraries.
require_once 'includes/lib/class-clixsy-toc-admin-api.php';

/**
 * Returns the main instance of clixsy_ToC to prevent the need to use globals.
 *
 * @since  1.0.0
 * @return object clixsy_ToC
 */
function clixsy_toc()
{
	$instance = clixsy_ToC::instance(__FILE__, '1.0.0');

	if (is_null($instance->settings)) {
		$instance->settings = clixsy_ToC_Settings::instance($instance);
	}

	return $instance;
}

clixsy_toc();



