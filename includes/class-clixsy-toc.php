<?php

/**
 * Main plugin class file.
 *
 * @package WordPress Plugin Template/Includes
 */

if (!defined('ABSPATH')) {
	exit;
}

/**
 * Main plugin class.
 */
class clixsy_ToC
{

	/**
	 * The single instance of clixsy_ToC.
	 *
	 * @var     object
	 * @access  private
	 * @since   1.0.0
	 */
	private static $_instance = null; //phpcs:ignore

	/**
	 * Local instance of clixsy_ToC_Admin_API
	 *
	 * @var clixsy_ToC_Admin_API|null
	 */
	public $admin = null;

	/**
	 * Settings class object
	 *
	 * @var     object
	 * @access  public
	 * @since   1.0.0
	 */
	public $settings = null;

	/**
	 * The version number.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $_version; //phpcs:ignore

	/**
	 * The token.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $_token; //phpcs:ignore

	/**
	 * The main plugin file.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $file;

	/**
	 * The main plugin directory.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $dir;

	/**
	 * The plugin assets directory.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $assets_dir;

	/**
	 * The plugin assets URL.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $assets_url;

	/**
	 * Suffix for JavaScripts.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $script_suffix;

	/**
	 * Constructor funtion.
	 *
	 * @param string $file File constructor.
	 * @param string $version Plugin version.
	 */
	public function __construct($file = '', $version = '1.0.0')
	{
		$this->_version = $version;
		$this->_token   = 'clixsy_toc';

		// Load plugin environment variables.
		$this->file       = $file;
		$this->dir        = dirname($this->file);
		$this->assets_dir = trailingslashit($this->dir) . 'assets';
		$this->assets_url = esc_url(trailingslashit(plugins_url('/assets/', $this->file)));

		$this->script_suffix = defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ? '' : '.min';

		register_activation_hook($this->file, array($this, 'install'));

		// Load frontend JS & CSS.
		add_action('wp_enqueue_scripts', array($this, 'enqueue_styles'), 10);
		add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts'), 10);



		// Load admin JS & CSS.
		add_action('admin_enqueue_scripts', array($this, 'admin_enqueue_scripts'), 10, 1);
		add_action('admin_enqueue_scripts', array($this, 'admin_enqueue_styles'), 10, 1);

		// Load API for generic admin functions.
		if (is_admin()) {
			$this->admin = new clixsy_ToC_Admin_API();
		}

		// Handle localisation.
		$this->load_plugin_textdomain();
		add_action('init', array($this, 'load_localisation'), 0);
	} // End __construct ()



	/**
	 * Load frontend CSS.
	 *
	 * @access  public
	 * @return void
	 * @since   1.0.0
	 * disable_styles variable from plugin options page
	 */


	public function enqueue_styles()
	{
		$disable_styles = get_option('wpt_disable_styles');
		if ($disable_styles) {
			return false;
		}
		wp_register_style($this->_token . '-frontend', esc_url($this->assets_url) . 'css/frontend.css', array(), $this->_version);
		wp_enqueue_style($this->_token . '-frontend');
	} // End enqueue_styles ()




	/**
	 * Load frontend Javascript.
	 *
	 * @access  public
	 * @return  void
	 * @since   1.0.0
	 */
	public function enqueue_scripts()
	{
		wp_register_script($this->_token . '-frontend', esc_url($this->assets_url) . 'js/frontend' . $this->script_suffix . '.js', array('jquery'), $this->_version, true);
		wp_enqueue_script($this->_token . '-frontend');
	} // End enqueue_scripts ()

	/**
	 * Admin enqueue style.
	 *
	 * @param string $hook Hook parameter.
	 *
	 * @return void
	 */
	public function admin_enqueue_styles($hook = '')
	{
		wp_register_style($this->_token . '-admin', esc_url($this->assets_url) . 'css/admin.css', array(), $this->_version);
		wp_enqueue_style($this->_token . '-admin');
	} // End admin_enqueue_styles ()

	/**
	 * Load admin Javascript.
	 *
	 * @access  public
	 *
	 * @param string $hook Hook parameter.
	 *
	 * @return  void
	 * @since   1.0.0
	 */
	public function admin_enqueue_scripts($hook = '')
	{
		wp_register_script($this->_token . '-admin', esc_url($this->assets_url) . 'js/admin' . $this->script_suffix . '.js', array('jquery'), $this->_version, true);
		wp_enqueue_script($this->_token . '-admin');
	} // End admin_enqueue_scripts ()

	/**
	 * Load plugin localisation
	 *
	 * @access  public
	 * @return  void
	 * @since   1.0.0
	 */
	public function load_localisation()
	{
		load_plugin_textdomain('clixsy-toc', false, dirname(plugin_basename($this->file)) . '/lang/');
	} // End load_localisation ()

	/**
	 * Load plugin textdomain
	 *
	 * @access  public
	 * @return  void
	 * @since   1.0.0
	 */
	public function load_plugin_textdomain()
	{
		$domain = 'clixsy-toc';

		$locale = apply_filters('plugin_locale', get_locale(), $domain);

		load_textdomain($domain, WP_LANG_DIR . '/' . $domain . '/' . $domain . '-' . $locale . '.mo');
		load_plugin_textdomain($domain, false, dirname(plugin_basename($this->file)) . '/lang/');
	} // End load_plugin_textdomain ()

	/**
	 * Main clixsy_ToC Instance
	 *
	 * Ensures only one instance of clixsy_ToC is loaded or can be loaded.
	 *
	 * @param string $file File instance.
	 * @param string $version Version parameter.
	 *
	 * @return Object clixsy_ToC instance
	 * @see clixsy_ToC()
	 * @since 1.0.0
	 * @static
	 */
	public static function instance($file = '', $version = '1.0.0')
	{
		if (is_null(self::$_instance)) {
			self::$_instance = new self($file, $version);
		}

		return self::$_instance;
	} // End instance ()

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone()
	{
		_doing_it_wrong(__FUNCTION__, esc_html(__('Cloning of clixsy_ToC is forbidden')), esc_attr($this->_version));
	} // End __clone ()

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup()
	{
		_doing_it_wrong(__FUNCTION__, esc_html(__('Unserializing instances of clixsy_ToC is forbidden')), esc_attr($this->_version));
	} // End __wakeup ()

	/**
	 * Installation. Runs on activation.
	 *
	 * @access  public
	 * @return  void
	 * @since   1.0.0
	 */
	public function install()
	{
		$this->_log_version_number();
	} // End install ()

	/**
	 * Log the plugin version number.
	 *
	 * @access  public
	 * @return  void
	 * @since   1.0.0
	 */
	private function _log_version_number()
	{ //phpcs:ignore
		update_option($this->_token . '_version', $this->_version);
	} // End _log_version_number ()

}
//Include additional styles to the wp_head
function clixsy_additional_styles()
{
	$custom_css = preg_replace(array('/\s{2,}/', '/[\t\n]/'), '', get_option('wpt_custom_css'));
	if (empty($custom_css)) {
		return false;
	}
	wp_enqueue_style('clixsy_toc_additional_styles', plugin_dir_url('') . 'clixsy-toc/assets/css/additional_styles.css');

	wp_add_inline_style('clixsy_toc_additional_styles', $custom_css);
}
add_action('wp_enqueue_scripts', 'clixsy_additional_styles');

//If plugin option disable_plugin checked
$disable_plugin = get_option('wpt_disable_plugin');
if ($disable_plugin) {
	return false;
}


function toc_shortcode($atts, $content = null)
{
	ob_start();
	shortcode_function();
	return ob_get_clean();
}
add_shortcode('toc', 'toc_shortcode');

function shortcode_function()
{

	$content = get_the_content('', false);

	if (strlen($content) > 0 || is_admin()) {
		preg_match_all("#<h(\d)[^>]*?(?:[^>]*?(?:id=\"([^\"]*?)\")[^>]*?)*?>(.*?)<[^>]*?/h\d>#i", $content, $headings, PREG_SET_ORDER);
		if (!empty($headings) || is_admin()) {
			$title = get_option('wpt_toc_title');
			$is_toggable = get_option('wpt_is_toggable');
?>


			<div class="clixsy-toc <?php echo (!$is_toggable ? 'not-toggable' : '') ?>">
				<div class="clixsy-toc__title <?php echo ($is_toggable ? 'toc_toggle' : '') ?>">
					<h6><?php echo $title ?>
						<?php $toggle_icon = get_option('wpt_toggle_icon') ?>
					</h6>
					<span class="toggle-icon"><img width="12px" src="<?php echo (!empty($toggle_icon) ? wp_get_attachment_image_url($toggle_icon) : plugin_dir_url('') . 'clixsy-toc/assets/img/toggle-icon.svg') ?>" alt=""></span>
				</div>
				<div class="clixsy-toc__collapse">
					<?php $type_of_markers = get_option('wpt_type_of_markers') ?: 'ul'; ?>
					<<?php echo $type_of_markers ?> class="clixsy-toc__content">
						<?php if (!empty($headings)) { ?>
							<?php foreach ($headings as $heading) { ?>
								<li itemprop="hasPart" itemscope itemtype="http://schema.org/Article">
									<a itemprop="url" href="#<?php echo !empty($heading[2]) ? $heading[2] : esc_attr(str_replace(' ', '-', strtolower(strip_tags($heading[3])))); ?>">
										<span itemprop="name"><?php echo strip_tags($heading[3]); ?></span>
									</a>
								</li>
							<?php } ?>
						<?php } ?>
					</<?php echo $type_of_markers ?>>
				</div>
			</div>

		<?php
		}
	}
}



function clixsy_filter_the_content($content)
{
	$toc_content = clixsy_toc_function();
	$toc_content .= $content;
	return $toc_content;
}
add_filter('the_content', 'clixsy_filter_the_content');

function clixsy_toc_function()
{
	//If page id on list with ID
	global $post;
	$post_id = $post->ID;
	$id_to_exclude = explode(',', get_option('wpt_id_to_exclude'));
	if (!empty($id_to_exclude) && in_array($post_id, $id_to_exclude)) {
		return false;
	}

	//If post type not in the clixsy_cpt field
	$clixsy_cpt = get_option('wpt_clixsy_cpt');
	$post_type = $post->post_type;
	if (!empty($clixsy_cpt) && !in_array($post_type, $clixsy_cpt)) {
		return false;
	}


	$content = get_the_content('', false);

	if (strlen($content) > 0 || is_admin()) {
		preg_match_all("#<h(\d)[^>]*?(?:[^>]*?(?:id=\"([^\"]*?)\")[^>]*?)*?>(.*?)<[^>]*?/h\d>#i", $content, $headings, PREG_SET_ORDER);
		if (!empty($headings) || is_admin()) {
			$title = get_option('wpt_toc_title');
			$is_toggable = get_option('wpt_is_toggable');
		?>

			<div class="clixsy-toc <?php echo (!$is_toggable ? 'not-toggable' : '') ?>">
				<div class="clixsy-toc__title <?php echo ($is_toggable ? 'toc_toggle' : '') ?>">
					<h6><?php echo $title ?>
						<?php $toggle_icon = get_option('wpt_toggle_icon') ?>
					</h6>
					<span class="toggle-icon"><img width="12px" src="<?php echo (!empty($toggle_icon) ? wp_get_attachment_image_url($toggle_icon) : plugin_dir_url('') . 'clixsy-toc/assets/img/toggle-icon.svg') ?>" alt=""></span>
				</div>
				<div class="clixsy-toc__collapse">
					<?php $type_of_markers = get_option('wpt_type_of_markers') ?: 'ul'; ?>
					<<?php echo $type_of_markers ?> class="clixsy-toc__content">
						<?php if (!empty($headings)) { ?>
							<?php foreach ($headings as $heading) { ?>
								<li itemprop="hasPart" itemscope itemtype="http://schema.org/Article">
									<a itemprop="url" href="#<?php echo !empty($heading[2]) ? $heading[2] : esc_attr(str_replace(' ', '-', strtolower(strip_tags($heading[3])))); ?>">
										<span itemprop="name"><?php echo strip_tags($heading[3]); ?></span>
									</a>
								</li>
							<?php } ?>
						<?php } ?>
					</<?php echo $type_of_markers ?>>
				</div>
			</div>

<?php
		}
	}
}



/*
 * Filter content to add ids to heading
 */


function clixsy_array_cpt()
{
	$args = array(
		'public'   => true,
		'_builtin' => false
	);
	$output = 'names'; // names or objects, note names is the default
	$operator = 'and'; // 'and' or 'or'
	$post_types = get_post_types($args, $output, $operator);
	$cpt_array = array();
	foreach ($post_types  as $post_type) {
		$cpt_array[] = $post_type;
	}
	$cpt_array = array('post' => 'posts', 'page' => 'pages') + array_combine($cpt_array, $cpt_array);
	return $cpt_array;
}
add_action('pre_get_posts', 'function_name');

function clixsy_toc_filter_content($content)
{

	if (strlen($content) > 0) {
		preg_match_all("#<h(\d)[^>]*?(?:[^>]*?(?:id=\"([^\"]*?)\")[^>]*?)*?>(.*?)<[^>]*?/h\d>#i", $content, $headings, PREG_SET_ORDER);

		foreach ($headings as $heading) {
			if (strpos($heading[0], 'id="') === false) {
				$new_heading = substr_replace($heading[0], ' id="' . esc_attr(str_replace(' ', '-', strtolower(strip_tags($heading[3])))) . '"', 3, 0);

				$content = str_replace($heading[0], $new_heading, $content);
			}
		}
	}

	return $content;
}
add_filter('the_content', 'clixsy_toc_filter_content', 20);


// Register block
function register_clixsy_toc_block()
{
	if (function_exists('acf_register_block_type')) {
		acf_register_block_type(array(
			'name'              => 'clixsy-toc',
			'title'             => 'Table of contents',
			'description'       => 'Show TOC.',
			'render_callback'   => 'clixsy_toc',
			'category'          => 'widgets',
			'icon'              => 'location-alt',
			'mode'              => 'preview',
			'keywords'          => array('clixsy-toc'),
		));
	}
}
add_action('acf/init', 'register_clixsy_toc_block');

// Declare acf
if (function_exists('acf_add_local_field_group')) {
	acf_add_local_field_group(array(
		'key'            => 'group_block_clixsy_toc',
		'title'          => 'Table of contents block',
		'location' => array(
			array(
				array(
					'param' => 'block',
					'operator' => '==',
					'value' => 'acf/clixsy-toc',
				),
			),
		),
		'fields'         => array()
	));
}

/*
 * Check if there is toc block in the content
 * Should be used inside loop
 */
function clixsy_is_content_block()
{
	$content = get_the_content('', false);

	if (strlen($content) > 0) {
		if (strpos($content, 'wp:acf/clixsy-toc') !== false) {
			return true;
		}
	}

	return false;
}
