/**
 * Plugin Template frontend js.
 *
 *  @package WordPress Plugin Template/JS
 */

jQuery(document).ready(function (e) {
  // Table of contents
  jQuery('.clixsy-toc__title').on('click', function () {
    if (!jQuery(this).hasClass('opened')) {
      // Open new
      jQuery(this)
        .next('.clixsy-toc__collapse')
        .height(
          jQuery(this).next('.clixsy-toc__collapse').find('.clixsy-toc__content').outerHeight(true),
        );
      jQuery(this).find('.fas').removeClass('fa-angle-down').addClass('fa-angle-up');
    } else {
      // Close current
      jQuery(this).next('.clixsy-toc__collapse').height(0);
      jQuery(this).find('.fas').removeClass('fa-angle-up').addClass('fa-angle-down');
    }
    jQuery(this).toggleClass('opened');
  });

  jQuery('.clixsy-toc').on('click', 'a', function (e) {
    e.preventDefault();

    history.pushState({}, '', this.href);

    var anchor = jQuery('[id="' + jQuery(this).attr('href').replace('#', '') + '"]');
    var addSpace = 130;
    var scrollVal = anchor.offset().top > 100 ? anchor.offset().top - addSpace : 0;

    window.scroll({ top: scrollVal, left: 0, behavior: 'smooth' });
  });
});
